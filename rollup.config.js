import babel from 'rollup-plugin-babel';
import resolve from 'rollup-plugin-node-resolve';
export default {
  entry: 'components/_main.js',
  dest: 'public/bundle.js',
  format: 'iife',
  sourceMap: 'inline',
  plugins: [
    resolve(),
    babel({
      exclude: 'node_modules/**',
    }),
  ],
};