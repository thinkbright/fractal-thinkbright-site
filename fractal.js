'use strict';

/*
* Require the path module
*/
const path = require('path');
const fs = require('fs');
/*
 * Require the Fractal module
 */
const fractal = module.exports = require('@frctl/fractal').create();

/*
 * Give your project a title.
 */
fractal.set('project.title', 'Fractal Thinkbright Site');

/*
 * Tell Fractal where to look for components.
 */
fractal.components.set('path', path.join(__dirname, 'components'));

/*
 * Tell Fractal where to look for documentation pages.
 */
fractal.docs.set('path', path.join(__dirname, 'docs'));

/*
 * Tell the Fractal web preview plugin where to look for static assets.
 */
fractal.web.set('static.path', path.join(__dirname, 'public'));

/**
 * Set Twig as templating engine
 */
fractal.components.engine('@frctl/twig');

/**
 * Set extensions of component files to .twig
 */
fractal.components.set('ext', '.twig');


fractal.components.set('default.context', 'default.config.js');

/**
 * Set BrowserSync options
 */
fractal.web.set('server.sync', true);
fractal.web.set('server.syncOptions', {
    open: true,
    files: ['components/**/*.twig', 'components/**/*.scss', 'public/*.css', 'public/*.js']
});

/**
 * Export components map
 */
function exportPaths() {
	const map = {};
	for (let item of fractal.components.flatten()) {
		map[`@${item.handle}`] = path.relative(process.cwd(), item.viewPath);
	}
	fs.writeFileSync('components-map.json', JSON.stringify(map, null, 2), 'utf8');
}

fractal.components.on('updated', function(){
    exportPaths();
});

fractal.cli.command('pathmap', function(opts, done){
    exportPaths();
    done();
});
