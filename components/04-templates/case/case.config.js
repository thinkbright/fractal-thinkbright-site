var colours = ["67767A", "355C66", "FF675C", "FFC95C", "85CC49"];
var tiles = [
    {
        linkedEntry:{
            tileBackgroundColor: "#FCFCFC",
            tileBody: "Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum",
            tileFontColor: "#111111",
            tileHeader: "HEADER",
            tileLinkurl: "http://link",
            tileLinkColor: "#111111",
            tileSubheader: "SUBHEADER",
            tileImage: {
                url: "http://via.placeholder.com/500/" + colours[Math.floor(Math.random()*4)] + "/000000",
                title: "placeholder",
            }
        },
        size: "small",
        column: "left"
    },{
        linkedEntry: {
            tileBackgroundColor: "#FCFCFC",
            tileBody: "Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum",
            tileFontColor: "#111111",
            tileHeader: "HEADER",
            tileLinkurl: "http://link",
            tileLinkColor: "#111111",
            tileSubheader: "SUBHEADER",
            tileImage: {
                url: "http://via.placeholder.com/500/" + colours[Math.floor(Math.random()*4)] + "/000000",
                title: "placeholder",
            }
        },
        size: "small",
        column: "right"
    }
];

var team = {
    heading: "Een team met een brede blik",
    subHeading: "Op het snijpunt van strategie, ontwerp en ontwikkeling",
    body: "Vanuit onze kennis en ervaring geven we onze mening en denken we kritisch mee, maar we weten ook wanneer het tijd is voor pragmatisme. Aan het einde van de rit leveren we geen abstracte concepten maar producten die werken."
};

var craftTeam = [
    {
        componentImage: {
            craftImage: {
                url: "http://via.placeholder.com/500x750/880000/ffffff?text=Rommert",
                title: "placeholder",
            },
            breakpoints: null,
            lazyLoad: false,
            imageClass: "c-image",
            containerClass: "c-image-container",
            padding: "",
        },
            profileSiteBackgroundColor: "red",
            name: "Rommert",
            profileSiteTitle:"koffie"
    },{
        componentImage: {
            craftImage: {
                url: "http://via.placeholder.com/500x750/008800/ffffff?text=Jacco",
                title: "placeholder",
            },
            breakpoints: null,
            lazyLoad: false,
            imageClass: "c-image",
            containerClass: "c-image-container",
            padding: "",
        },
            profileSiteBackgroundColor: "green",
            name: "Jacco",
            profileSiteTitle:"thee"
    },{
        componentImage: {
            craftImage: {
                url: "http://via.placeholder.com/500x750/000088/ffffff?text=Marijn",
                title: "placeholder",
            },
            breakpoints: null,
            lazyLoad: false,
            imageClass: "c-image",
            containerClass: "c-image-container",
            padding: "",
        },
            profileSiteBackgroundColor: "blue",
            name: "Marijn",
            profileSiteTitle:"water"
    }
];

var blocks = [
    {
        type: "text",
        size: "full",
        align: "left",
        header: "Two Column",
        subtitle: "This text has two columns",
        body: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
    },{
        type: "image",
        size: "half",
        align: "left",
        image: {
            image: {
                url: "http://via.placeholder.com/500?text=Left",
                title: "placeholder"
            }
        }
    },{
        type: "image",
        size: "half",
        align: "right",
        image: {
            image: {
                url: "http://via.placeholder.com/500?text=Right",
                title: "placeholder"
            }
        }
    },{
        type: "text",
        size: "half",
        align: "left",
        header: "Left",
        subtitle: "This text is left-aligned",
        body: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
    },{
        type: "text",
        size: "half",
        align: "right",
        header: "Right",
        subtitle: "This text is right-aligned",
        body: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
    },{
        type: "image",
        size: "full",
        align: "left",
        image: {
            image: {
                url: "http://via.placeholder.com/1000x500?text=Full",
                title: "placeholder"
            }
        }
    },{
        type: "text",
        size: "half",
        align: "left",
        header: "Left",
        subtitle: "This text is left-aligned",
        body: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
    },{
        type: "image",
        size: "half",
        align: "right",
        image: {
            image: {
                url: "http://via.placeholder.com/500?text=Right",
                title: "placeholder"
            }
        }
    },{
        type: "image",
        size: "half",
        align: "left",
        image: {
            image: {
                url: "http://via.placeholder.com/500?text=Left",
                title: "placeholder"
            }
        }
    },{
        type: "text",
        size: "half",
        align: "right",
        header: "Right",
        subtitle: "This text is right-aligned",
        body: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
    }
];

module.exports = {
    title: "Home",
    status: "wip",
    context: {
        globalContact: {
            globalPhoneNumber: "030 227 01 52",
            globalEmailAddress: "hello@thinkbright.nl"
        },
        footer: {
            siteName: "Thinkbright",
            globalAddress: "Hooghiemstraplein 73",
            globalZipCode: "3514 AX",
            globalCity: "Utrecht",
            globalPhoneNumber: "030 227 01 52",
            globalEmailAddress: "hello@thinkbright.nl",
            globalCompanyRegistrationId: "KVK: 30252576",
            globalVatNumber: "BTW: NL820336026B01",
            craftImage: {
                url: "http://placekitten.com/1024/1024",
                title: "Cat"
            }
        },
        entry: {
            pageHeroImage: {
                url: "http://placekitten.com/1024/1024",
                title: "placeitten"
            },
            pageTitle: "Page Title",
            pageSubTitle: "Page Subtitle",
            pageIntro: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
            pageBlocks: blocks,
            pageMosaic: tiles,
        }
    }
};