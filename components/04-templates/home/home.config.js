var colours = ["67767A", "355C66", "FF675C", "FFC95C", "85CC49"];
var tiles = [
    {
        linkedEntry:{
            tileBackgroundColor: "#FCFCFC",
            tileBody: "Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum",
            tileFontColor: "#111111",
            tileHeader: "HEADER",
            tileLinkurl: "http://link",
            tileLinkColor: "#111111",
            tileSubheader: "SUBHEADER",
            tileImage: {
                url: "http://via.placeholder.com/500/" + colours[Math.floor(Math.random()*4)] + "/000000",
                title: "placeholder",
            }
        },
        size: "wide",
        column: "left"
    },{
        linkedEntry: {
            tileBackgroundColor: "#FCFCFC",
            tileBody: "Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum",
            tileFontColor: "#111111",
            tileHeader: "HEADER",
            tileLinkurl: "http://link",
            tileLinkColor: "#111111",
            tileSubheader: "SUBHEADER",
            tileImage: {
                url: "http://via.placeholder.com/500/" + colours[Math.floor(Math.random()*4)] + "/000000",
                title: "placeholder",
            }
        },
        size: "tall",
        column: "right"
    },{
        linkedEntry:{
            tileBackgroundColor: "#FCFCFC",
            tileBody: "Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum",
            tileFontColor: "#111111",
            tileHeader: "HEADER",
            tileLinkurl: "http://link",
            tileLinkColor: "#111111",
            tileSubheader: "SUBHEADER",
            tileImage: {
                url: "http://via.placeholder.com/500/" + colours[Math.floor(Math.random()*4)] + "/000000",
                title: "placeholder",
            }
        },
        size: "small",
        column: "left"
    },{
        linkedEntry:{
            tileBackgroundColor: "#FCFCFC",
            tileBody: "Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum",
            tileFontColor: "#111111",
            tileHeader: "HEADER",
            tileLinkurl: "http://link",
            tileLinkColor: "#111111",
            tileSubheader: "SUBHEADER",
            tileImage: {
                url: "http://via.placeholder.com/500/" + colours[Math.floor(Math.random()*4)] + "/000000",
                title: "placeholder",
            }
        },
        size: "small",
        column: "left"
    },
];

var team = {
    heading: "Een team met een brede blik",
    subHeading: "Op het snijpunt van strategie, ontwerp en ontwikkeling",
    body: "Vanuit onze kennis en ervaring geven we onze mening en denken we kritisch mee, maar we weten ook wanneer het tijd is voor pragmatisme. Aan het einde van de rit leveren we geen abstracte concepten maar producten die werken."
};
var craftTeam= [
    {
        componentImage: {
            craftImage: {
                url: "http://via.placeholder.com/500x750/880000/ffffff?text=Rommert",
                title: "placeholder",
            },
            breakpoints: null,
            lazyLoad: false,
            imageClass: "c-image",
            containerClass: "c-image-container",
            padding: "",
        },
            profileSiteBackgroundColor: "red",
            name: "Rommert",
            profileSiteTitle:"koffie"
    },{
        componentImage: {
            craftImage: {
                url: "http://via.placeholder.com/500x750/008800/ffffff?text=Jacco",
                title: "placeholder",
            },
            breakpoints: null,
            lazyLoad: false,
            imageClass: "c-image",
            containerClass: "c-image-container",
            padding: "",
        },
            profileSiteBackgroundColor: "green",
            name: "Jacco",
            profileSiteTitle:"thee"
    },{
        componentImage: {
            craftImage: {
                url: "http://via.placeholder.com/500x750/000088/ffffff?text=Marijn",
                title: "placeholder",
            },
            breakpoints: null,
            lazyLoad: false,
            imageClass: "c-image",
            containerClass: "c-image-container",
            padding: "",
        },
            profileSiteBackgroundColor: "blue",
            name: "Marijn",
            profileSiteTitle:"water"
    }];

module.exports = {
    title: "Home",
    status: "wip",
    context: {
        globalContact: {
            globalPhoneNumber: "030 227 01 52",
            globalEmailAddress: "hello@thinkbright.nl"
        },
        opener: {
            title: "INTERNETBUREAU VOOR DOORDACHTE CONCEPTEN",
            subTitle: "Als kritische denkers en creatieve makers zetten we onze tanden in uitdagende online vraagstukken. Samen naar de kern van het probleem en dan de mouwen opstropen om de oplossing te realiseren.",
            inBetween: "van strategie tot ontwikkeling"
        },
        pageMosaic: tiles,
        team: team,
        craftTeam: craftTeam,
        workflow: {
            heading: "Ons medium is online",
            subHeading: "Continu in beweging en altijd uitdagend",
            body: [
                "We ontleden vraagstukken en vragen door tot we bij de kern komen. Van daaruit gaan we op zoek naar oplossingen die passen binnen de context van de markt van het product of de dienst.",
                "We bedenken niet alleen concepten, maar werken die ook visueel en technisch uit. Elk project heeft een unieke uitdaging, daarom geloven we in oplossingen op maat.",
                "Na oplevering monitoren we statistieken over bezoekers, inhoud en zoekmachine-optimalisatie zodat er op basis van objectieve data kunnen zien waar potentie voor verbetering en groei zit."
                
            ]
        },
        network: {
            heading: "+ Ons Netwerk",
            body: "We werken voor verschillende opdrachten graag samen met specialisten in ons netwerk. Experts in vormgeving, techniek, tekst, fotografie en filmcommunicatie bijvoorbeeld."
        },
        footer: {
            siteName: "Thinkbright",
            globalAddress: "Hooghiemstraplein 73",
            globalZipCode: "3514 AX",
            globalCity: "Utrecht",
            globalPhoneNumber: "030 227 01 52",
            globalEmailAddress: "hello@thinkbright.nl",
            globalCompanyRegistrationId: "KVK: 30252576",
            globalVatNumber: "BTW: NL820336026B01",
            craftImage: {
                url: "http://placekitten.com/1024/1024",
                title: "Cat"
            }
        }
    }
};