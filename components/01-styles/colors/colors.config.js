module.exports = {
    title: "",
    status: "ready",
    context: {
        colors : [
            {name: "black"},       
            {name: "gray"},     
            {name: "gray-dark"},
            {name: "gray-light"}, 
            {name: "blue"},        
            {name: "blue-dark"},   
            {name: "blue-light"},  
            {name: "brown"},       
            {name: "red"},         
            {name: "red-dark"},    
            {name: "red-light"},   
            {name: "yellow"},      
            {name: "yellow-dark"}, 
            {name: "yellow-light"},
            {name: "yellow-bg"},   
            {name: "green"},       
            {name: "green-dark"},  
            {name: "green-light"}, 
            {name: "green-bg"},    
            {name: "white"}       
        ]
    }
};