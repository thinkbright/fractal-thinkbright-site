var weights = ['light', 'regular', 'bold'];

var fonts = { 'sansa-soft-pro': 'Sansa Soft Pro', 'sansa-soft-pro--condensed': 'Sansa Soft Condensed Pro', 'carrara': 'Carrara'};

module.exports = {
    title: 'Fonts',
    status: 'ready',
    context: {
        weights: weights,
        fonts: fonts
    }
};