module.exports = {
    title: "Headings",
    status: "ready",
    context: {
        h1: "H1 header",
        h2: "H2 header",
        h3: "H3 header",
        h4: "H4 header",
        h5: "H5 header",
        h6: "H6 header",
        sub: "Sub Header"
    },
    notes: "Heading styles are defined in _tb-framework/_headings.scss"
};