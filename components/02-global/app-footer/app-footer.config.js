module.exports = {
    title: "Footer",
    status: "ready",
    context: {
        footer: {
            siteName: "Thinkbright",
            globalAdress: "Hooghiemstraplein 73",
            globalZipCode: "3514 AX",
            globalCity: "Utrecht",
            globalPhoneNumber: "030 227 01 52",
            globalEmailAddress: "hello@thinkbright.nl",
            globalRegistrationId: "KVK: 30252576",
            globalVatNumber: "BTW: NL8203360"
        }
    }
};