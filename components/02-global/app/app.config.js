module.exports = {
    title: "App Base",
    status: "ready",
    context: {
        globalContact: {
            globalPhoneNumber: "030 227 01 52",
            globalEmailAddress: "hello@thinkbright.nl"
        },
        footer: {
            siteName: "Thinkbright",
            globalAddress: "Hooghiemstraplein 73",
            globalZipCode: "3514 AX",
            globalCity: "Utrecht",
            globalPhoneNumber: "030 227 01 52",
            globalEmailAddress: "hello@thinkbright.nl",
            globalCompanyRegistrationId: "KVK: 30252576",
            globalVatNumber: "BTW: NL8203360",
            craftImage: {
                url: "http://placekitten.com/1024/1024",
                title: "Cat"
            }
        },
        craft: {
            request: {
                isAjax: false
            }
        },
        isFractal: "true"
    }
};