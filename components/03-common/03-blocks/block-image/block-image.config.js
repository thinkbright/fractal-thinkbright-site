module.exports = {
    title: "Block | Image",
    status: "wip",
    context: {
        blockImage: {
            matrixBlock: {
                image: {
                    image: {
                        url: "http://via.placeholder.com/200x200",
                        title: "placeholder"
                    }
                }
            },
            sizeClass: "o-width-r-half"
        },
    }
};