module.exports = {
    title: "Blocks",
    status: "wip",
    context: {
        componentBlocks: {
            craftComponentBlocks: [
                {
                    type: "text",
                    size: "full",
                    align: "left",
                    header: "Two Column",
                    subtitle: "This text has two columns",
                    body: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
                },{
                    type: "image",
                    size: "half",
                    align: "left",
                    image: {
                        image: {
                            url: "http://via.placeholder.com/500?text=Left",
                            title: "placeholder"
                        }
                    }
                },{
                    type: "image",
                    size: "half",
                    align: "right",
                    image: {
                        image: {
                            url: "http://via.placeholder.com/500?text=Right",
                            title: "placeholder"
                        }
                    }
                },{
                    type: "text",
                    size: "half",
                    align: "left",
                    header: "Left",
                    subtitle: "This text is left-aligned",
                    body: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
                },{
                    type: "text",
                    size: "half",
                    align: "right",
                    header: "Right",
                    subtitle: "This text is right-aligned",
                    body: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
                },{
                    type: "image",
                    size: "full",
                    align: "left",
                    image: {
                        image: {
                            url: "http://via.placeholder.com/1000x500?text=Full",
                            title: "placeholder"
                        }
                    }
                },{
                    type: "text",
                    size: "half",
                    align: "left",
                    header: "Left",
                    subtitle: "This text is left-aligned",
                    body: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
                },{
                    type: "image",
                    size: "half",
                    align: "right",
                    image: {
                        image: {
                            url: "http://via.placeholder.com/500?text=Right",
                            title: "placeholder"
                        }
                    }
                },{
                    type: "image",
                    size: "half",
                    align: "left",
                    image: {
                        image: {
                            url: "http://via.placeholder.com/500?text=Left",
                            title: "placeholder"
                        }
                    }
                },{
                    type: "text",
                    size: "half",
                    align: "right",
                    header: "Right",
                    subtitle: "This text is right-aligned",
                    body: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
                }
            ]
        }
    }
};