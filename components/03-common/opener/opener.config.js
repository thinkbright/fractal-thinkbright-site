module.exports = {
    title: "Opener",
    status: 'ready',
    context: {
        opener: {
            title: "INTERNETBUREAU VOOR DOORDACHTE CONCEPTEN",
            subTitle: "Als kritische denkers en creatieve makers zetten we onze tanden in uitdagende online vraagstukken. Samen naar de kern van het probleem en dan de mouwen opstropen om de oplossing te realiseren.",
            inBetween: "van strategie tot ontwikkeling"
        }
    }
};