module.exports = {
    title: "Network",
    status: "ready",
    context: {
        network: {
            heading: "+ Ons Netwerk",
            body: "We werken voor verschillende opdrachten graag samen met specialisten in ons netwerk. Experts in vormgeving, techniek, tekst, fotografie en filmcommunicatie bijvoorbeeld."
        } 
    }
};