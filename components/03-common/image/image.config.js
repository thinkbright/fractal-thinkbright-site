module.exports = {
    title: "Image",
    status: "ready",
    context: {
        componentImage: {
            craftImage: {
                url: "http://via.placeholder.com/500",
                title: "placeholder",
            },
            breakpoints: null,
            lazyLoad: false,
            imageClass: "c-image",
            containerClass: "c-image-container",
            padding: "",

        }
    }
};