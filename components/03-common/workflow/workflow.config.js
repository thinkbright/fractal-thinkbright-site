module.exports = {
    title: "Workflow",
    status: "wip",
    context: {
        workflow: {
            heading: "Ons medium is online",
            subHeading: "Continu in beweging en altijd uitdagend",
            body: [
                "We ontleden vraagstukken en vragen door tot we bij de kern komen. Van daaruit gaan we op zoek naar oplossingen die passen binnen de context van de markt van het product of de dienst.",
                "We bedenken niet alleen concepten, maar werken die ook visueel en technisch uit. Elk project heeft een unieke uitdaging, daarom geloven we in oplossingen op maat.",
                "Na oplevering monitoren we statistieken over bezoekers, inhoud en zoekmachine-optimalisatie zodat er op basis van objectieve data kunnen zien waar potentie voor verbetering en groei zit."
                
            ]
        }
    }
};