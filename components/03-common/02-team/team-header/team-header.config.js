module.exports = {
    title: "Team | Header",
    status: "wip",
    context: {
        team: {
            heading: "Een team met een brede blik",
            subHeading: "Op het snijpunt van strategie, ontwerp en ontwikkeling",
            body: "Vanuit onze kennis en ervaring geven we onze mening en denken we kritisch mee, maar we weten ook wanneer het tijd is voor pragmatisme. Aan het einde van de rit leveren we geen abstracte concepten maar producten die werken."
        }
    }
};