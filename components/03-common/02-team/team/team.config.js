module.exports = {
    title: "Team | Complete",
    status: "wip",
    context:{
        team: {
            heading: "Een team met een brede blik",
            subHeading: "Op het snijpunt van strategie, ontwerp en ontwikkeling",
            body: "Vanuit onze kennis en ervaring geven we onze mening en denken we kritisch mee, maar we weten ook wanneer het tijd is voor pragmatisme. Aan het einde van de rit leveren we geen abstracte concepten maar producten die werken."
        },
        craftTeam: [
            {
                componentImage: {
                    craftImage: {
                        url: "http://via.placeholder.com/500x750/880000/ffffff?text=Rommert",
                        title: "placeholder",
                    },
                    breakpoints: null,
                    lazyLoad: false,
                    imageClass: "c-image",
                    containerClass: "c-image-container",
                    padding: "",
                },
                    profileSiteBackgroundColor: "red",
                    name: "Rommert",
                    profileSiteTitle:"koffie"
            },{
                componentImage: {
                    craftImage: {
                        url: "http://via.placeholder.com/500x750/008800/ffffff?text=Jacco",
                        title: "placeholder",
                    },
                    breakpoints: null,
                    lazyLoad: false,
                    imageClass: "c-image",
                    containerClass: "c-image-container",
                    padding: "",
                },
                    profileSiteBackgroundColor: "green",
                    name: "Jacco",
                    profileSiteTitle:"thee"
            },{
                componentImage: {
                    craftImage: {
                        url: "http://via.placeholder.com/500x750/000088/ffffff?text=Marijn",
                        title: "placeholder",
                    },
                    breakpoints: null,
                    lazyLoad: false,
                    imageClass: "c-image",
                    containerClass: "c-image-container",
                    padding: "",
                },
                    profileSiteBackgroundColor: "blue",
                    name: "Marijn",
                    profileSiteTitle:"water"
            }]
    }
};