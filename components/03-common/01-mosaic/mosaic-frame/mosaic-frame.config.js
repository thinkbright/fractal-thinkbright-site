module.exports = {
    title: "Mosaic Frame",
    status: "ready",
    notes: "This component does not have any context associated with it. The Mosaic component passes the data nessecary to create the rows.\n\nThis component simply contructs the right divs according to the contents of the rows."
};