<!--
Expects object named 'pageMosaic'

This object is an array with all mosaic tiles in order.
Each tile has a linkEntry which defines how the entry should be represented as a tile.
Forthermore, each tile has the following properties:

    * size      <small | tall | wide>   :   Dimensions of the tile. Small = 1x1, tall=1x2, wide=2x1.
    * column    <left | right>          :   Which column of the mosaic a tile should be in,
                                            has no effect when size is set to 'wide'.
-->

<div class="c-mosaic">
    {% set processed = [] %}
    {% set left = [] %}
    {% set right = [] %}
    {% for tile in pageMosaic %}
        {% set linkedEntry = tile.linkedEntry[0] %}
        {% set type = tile.size %}
        {% set column = tile.column %}
        {% set mosaicTile = {
          backgroundColor:  linkedEntry.tileBackgroundColor|default("#000000"),
          body:             linkedEntry.tileBody|default(""),

          caseHeroImage:    linkedEntry.tileImage[0]|default(""),

          fontColor:        linkedEntry.tileFontColor|default("#000000"),

          header:           linkedEntry.tileHeader|default(""),

          linkUrl:          linkedEntry.tileLinkUrl|default(linkedEntry.url|default('')),

          linkColor:        linkedEntry.tileLinkColor|default("#000000"),

          subHeader:        linkedEntry.tileSubheader|default(""),

          type:             type,
          column:           column

        } %}
        {% if mosaicTile.type == 'wide' %}
            {% if left|length or right|length %}
                {% include '@mosaic-frame' with {mosaicRow: {left: left, right: right, wide: ''} } %} 
            {% endif %}
            {% include '@mosaic-frame' with {mosaicRow: {left: '', right: '', wide: mosaicTile } } %}
            {% set left = [] %}
            {% set right = [] %}
        {% elseif mosaicTile.column == 'left' %}
            {% set left = left|merge([mosaicTile]) %}
        {% else %}
            {% set right = right|merge([mosaicTile]) %}
        {% endif %}
    {% endfor %}
    {% include '@mosaic-frame' with {mosaicRow: {left: left, right: right, wide: ''} } %} 
</div>
