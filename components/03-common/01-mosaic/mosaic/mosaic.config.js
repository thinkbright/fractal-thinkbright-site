var colours = ["67767A", "355C66", "FF675C", "FFC95C", "85CC49"];

var tiles = [
    {
        linkedEntry:{
            tileBackgroundColor: "#FCFCFC",
            tileBody: "Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum",
            tileFontColor: "#111111",
            tileHeader: "HEADER",
            tileLinkurl: "http://link",
            tileLinkColor: "#111111",
            tileSubheader: "SUBHEADER",
            tileImage: {
                url: "http://via.placeholder.com/500/" + colours[Math.floor(Math.random()*4)] + "/000000",
                title: "placeholder",
            }
        },
        size: "wide",
        column: "left"
    },{
        linkedEntry: {
            tileBackgroundColor: "#FCFCFC",
            tileBody: "Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum",
            tileFontColor: "#111111",
            tileHeader: "HEADER",
            tileLinkurl: "http://link",
            tileLinkColor: "#111111",
            tileSubheader: "SUBHEADER",
            tileImage: {
                url: "http://via.placeholder.com/500/" + colours[Math.floor(Math.random()*4)] + "/000000",
                title: "placeholder",
            }
        },
        size: "tall",
        column: "right"
    },{
        linkedEntry:{
            tileBackgroundColor: "#FCFCFC",
            tileBody: "Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum",
            tileFontColor: "#111111",
            tileHeader: "HEADER",
            tileLinkurl: "http://link",
            tileLinkColor: "#111111",
            tileSubheader: "SUBHEADER",
            tileImage: {
                url: "http://via.placeholder.com/500/" + colours[Math.floor(Math.random()*4)] + "/000000",
                title: "placeholder",
            }
        },
        size: "small",
        column: "left"
    },{
        linkedEntry:{
            tileBackgroundColor: "#FCFCFC",
            tileBody: "Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum",
            tileFontColor: "#111111",
            tileHeader: "HEADER",
            tileLinkurl: "http://link",
            tileLinkColor: "#111111",
            tileSubheader: "SUBHEADER",
            tileImage: {
                url: "http://via.placeholder.com/500/" + colours[Math.floor(Math.random()*4)] + "/000000",
                title: "placeholder",
            }
        },
        size: "small",
        column: "left"
    },
];

module.exports = {
title: "Mosaic",
status: "ready",
context: {
    pageMosaic: tiles
    }
};