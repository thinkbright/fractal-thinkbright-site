// Abstract:
// Animate a tile within the mosaic to the full page content.
//
// Init: search for c-tile class on page and add click event to all a-elements
//
// Click: search for element with c-tile class in content and:
// * clone element
// * load href target url
// * animate tile to full page
// * wait for 'animation complete' and 'target loaded'
// * remove clone
//

import Promise from 'promise-polyfill'

import D from '../../_common/scripts/dom'
import {map, omit} from '../../_common/scripts/helpers'
import {router} from '../../_common/scripts/router'

let body = D('body')

initTiles()
router.on('page:onUpdate', initTiles)

/**
 * Sets up tile functionality and transitions
 * @param  {Dombo} content A Dombo object
 * @return {Dombo}         Returns a Dombo object with events and transitions
 */
export function initTiles (content) {
  console.info('Initialized tiles...', content)

  let tiles = content
    ? content.find('.js-tile:not(.js-tile-clone)')
    : D('.js-tile:not(.js-tile-clone)')

  // Loop through tiles and add transitions to cloned tiles
  return tiles.each(node => {
    let tile = D(node)
    let link = tile.parent('a')

    link && link.on('click', event => {
      event.preventDefault()

      let clone = tile.clone(true)
      let styleCache = omit(['bottom', 'right'], tile[0].getBoundingClientRect())

      styleCache.position = 'fixed'

      clone.data('styleCache', JSON.stringify(map(toPx, styleCache)))

      clone.css(styleCache)
      clone.addClass('js-tile-clone')

      body.addClass('u-fixed').append(clone)

      let transitionEndPromise = new Promise(resolve => {
        clone.once('animationend', resolve)
      })

      let routerUpdatePromise = new Promise(resolve => {
        router.on('page:onUpdate', resolve)
      })

      Promise.all([transitionEndPromise, routerUpdatePromise])
        .then(() => {
          // Similarly, dom changes need to be postponed to fix a flicker
          // after loading the new page.
          // ~50ms seems to be the magic number.
          setTimeout(() => {
            body.removeClass('u-fixed')
            clone.remove()
          }, 50)
        })
    })
  })
}

function toPx (x) {
  return typeof x === 'number' ? x + 'px' : x
}

D(window).on('keydown', event => {
  event = event || window.event

  let code = event.keyCode || event.which

  if (code === 27) {
    let clones = D('.js-tile-clone')
    clones.each(node => {
      let clone = D(node)
      let styleCache = JSON.parse(clone.data('styleCache'))

      clone.css(styleCache)

      clone.once('transitionend', () => {
        D('body').removeClass('fixed')
        clone.remove()
      })
    })
  }
})
