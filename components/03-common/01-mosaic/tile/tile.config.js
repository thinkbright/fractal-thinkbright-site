module.exports = {
    title: "Tile",
    status: "ready",
    context: {
        mosaicTile: {
            type: "wide",
            linkUrl: "http://via.placehold.com/1024",
            backgroundColor: "#FCFCFC",
            fontColor: "#111111",
            header: "Header",
            subHeader: "Sub-Header",
            body: "This is the body. Some text. More text. Lorem Ipsum andsoforth",
            caseHeroImage: {
                url: "http://via.placeholder.com/500",
                title: "placeholder",
            }
        },
    }
};