## USAGE

#### OPTIONAL
Install fractal cli tool

```
npm install -save @frctl/fractal
```

#### RUN FRACTAL SERVER
Runs on node:v8.X


```
npm isntall
npm run build
npm run start
```

browsersync will be available at the port shown in your terminal (usally 3002).


#### DEVELOPMENT

- Add a folder for your component under the right folder: `components/<01-styles|02-layouts|etc.>`
- Add the neccesary `.twig`, `.scss` (prefixed with `_`), and `.config.<js|json|yml>` files
- Add the `.scss` to `main.scss` found at the `components` folder

**Note, twig.js does not support all Twig tags as of yet. see the twig.js docs for more info**
